var statInfo;
var weaponInfo;
var armorInfo;
var ringInfo;

var CATEGORY;

window.onload =
    function () {
        readDetails();
        fillStats(statInfo);
        fillWeapons(weaponInfo);
        fillArmor(armorInfo);
        fillRings(ringInfo);
    };

function fillStats(statInfo) {
    var data = JSON.parse(statInfo);
    var STAT_VIT = StringCheck(JSON.stringify(data.VIT));
    var STAT_ATT = StringCheck(JSON.stringify(data.ATT));
    var STAT_END = StringCheck(JSON.stringify(data.END));
    var STAT_STR = StringCheck(JSON.stringify(data.STR));
    var STAT_DEX = StringCheck(JSON.stringify(data.DEX));
    var STAT_RES = StringCheck(JSON.stringify(data.RES));
    var STAT_INT = StringCheck(JSON.stringify(data.INT));
    var STAT_FTH = StringCheck(JSON.stringify(data.FAI));

    document.getElementById("VIT_VAL").innerHTML = STAT_VIT;
    document.getElementById("ATT_VAL").innerHTML = STAT_ATT;
    document.getElementById("END_VAL").innerHTML = STAT_END;
    document.getElementById("STR_VAL").innerHTML = STAT_STR;
    document.getElementById("DEX_VAL").innerHTML = STAT_DEX;
    document.getElementById("RES_VAL").innerHTML = STAT_RES;
    document.getElementById("INT_VAL").innerHTML = STAT_INT;
    document.getElementById("FTH_VAL").innerHTML = STAT_FTH;
}

function fillWeapons(weaponInfo) {
    CATEGORY = "WEAPON";
    var data = JSON.parse(weaponInfo);
    var R_HAND_ONE = removeUpgradeLevel(StringCheck(JSON.stringify(data.R1)));
    var R_HAND_TWO = removeUpgradeLevel(StringCheck(JSON.stringify(data.R2)));
    var L_HAND_ONE = removeUpgradeLevel(StringCheck(JSON.stringify(data.L1)));
    var L_HAND_TWO = removeUpgradeLevel(StringCheck(JSON.stringify(data.L2)));

    var r_one_element = document.getElementById("RIGHT_ONE");
    var r_two_element = document.getElementById("RIGHT_TWO");
    var l_one_element = document.getElementById("LEFT_ONE");
    var l_two_element = document.getElementById("LEFT_TWO");

    document.getElementById("RIGHT_HAND_ONE").innerHTML = R_HAND_ONE;
    document.getElementById("RIGHT_HAND_TWO").innerHTML = R_HAND_TWO;
    document.getElementById("LEFT_HAND_ONE").innerHTML = L_HAND_ONE;
    document.getElementById("LEFT_HAND_TWO").innerHTML = L_HAND_TWO;

    if (hasBackground(R_HAND_ONE) === true) {
        setBackground(r_one_element, linkify(R_HAND_ONE, CATEGORY));
    }
    if (hasBackground(R_HAND_TWO) === true) {
        setBackground(r_two_element, linkify(R_HAND_TWO, CATEGORY));
    }
    if (hasBackground(L_HAND_ONE) === true) {
        setBackground(l_one_element, linkify(L_HAND_ONE, CATEGORY));
    }
    if (hasBackground(L_HAND_TWO) === true) {
        setBackground(l_two_element, linkify(L_HAND_TWO, CATEGORY));
    }

}

function fillArmor(armorInfo) {
    CATEGORY = "ARMOR";
    var data = JSON.parse(armorInfo);
    var HEAD = StringCheck(JSON.stringify(data.head));
    var CHEST = StringCheck(JSON.stringify(data.chest));
    var GLOVES = StringCheck(JSON.stringify(data.gloves));
    var LEGS = StringCheck(JSON.stringify(data.legs));

    var head_element = document.getElementById("HEAD");
    var chest_element = document.getElementById("CHEST");
    var gloves_element = document.getElementById("GLOVES");
    var legs_element = document.getElementById("LEGS");

    document.getElementById("HEAD_VAL").innerHTML = HEAD;
    document.getElementById("CHEST_VAL").innerHTML = CHEST;
    document.getElementById("GLOVES_VAL").innerHTML = GLOVES;
    document.getElementById("LEGS_VAL").innerHTML = LEGS;

    if (hasBackground(HEAD) === true) {
        setBackground(head_element, linkify(HEAD, 'HEAD'));
    }
    if (hasBackground(CHEST) === true) {
        setBackground(chest_element, linkify(CHEST, 'CHEST'));
    }
    if (hasBackground(GLOVES) === true) {
        setBackground(gloves_element, linkify(GLOVES, 'HANDS'));
    }
    if (hasBackground(LEGS) === true) {
        setBackground(legs_element, linkify(LEGS, 'LEGS'));
    }
}

function fillRings(ringInfo) {
    CATEGORY = "RINGS";
    var data = JSON.parse(ringInfo);
    var RING_ONE = StringCheck(JSON.stringify(data.ring1));
    var RING_TWO = StringCheck(JSON.stringify(data.ring2));

    var ring_one_element = document.getElementById("RING_ONE");
    var ring_two_element = document.getElementById("RING_TWO");

    document.getElementById("RING_ONE_VAL").innerHTML = RING_ONE;
    document.getElementById("RING_TWO_VAL").innerHTML = RING_TWO;

    if (hasBackground(RING_ONE) === true) {
        setBackground(ring_one_element, linkify(RING_ONE, CATEGORY));
    }
    if (hasBackground(RING_TWO) === true) {
        setBackground(ring_two_element, linkify(RING_TWO, CATEGORY));
    }
}

function readDetails() {
    var data = JSON.parse(JSON.stringify(CHARACTER_DETAILS));
    statInfo = JSON.stringify(data.stats);
    weaponInfo = JSON.stringify(data.weapons);
    armorInfo = JSON.stringify(data.armor);
    ringInfo = JSON.stringify(data.rings);
}

function StringCheck(stringToCheck) {
    if (isNaN(stringToCheck * 1)) {
        return stringToCheck.substring(1, (stringToCheck.length - 1));
    } else {
        return stringToCheck;
    }
}

function hasBackground(item) {
    var invalid_items = ["Naked", "Bare Fist", "No Ring"];
    if (invalid_items.indexOf(item) > -1) {
        return false;
    } else {
        return true;
    }
}

function linkify(item, category) {
    var item_type = "";
    switch (category) {
        case "WEAPON":
            if (WEAPON_DICTIONARY[item] != undefined){
                item_type = WEAPON_DICTIONARY[item];
            } else{
                item_type = SHIELD_DICTIONARY[item];
            }
            break;
        default:
            item_type = category.toLowerCase();
    }

    item = item.toLowerCase();
    item = item.replace('\'', '-');
    item = item.replaceAll(' ', '-');

    var pageItem = item;
    var pageImgItem = item;
    

    var itemLinks = ["", ""];

    var pageLinkStart = "http://darksouls.wikidot.com/";


    var imageLinkStart = "http://darksouls.wdfiles.com/local--files/";
    var imageLinkEnd = ".png";

    itemLinks[0] = pageLinkStart + pageItem;
    itemLinks[1] = imageLinkStart + item_type + "/" + pageImgItem + imageLinkEnd;

    // Special Use Case For Skull Lantern Image Being Hosted On Imgur
    if(pageImgItem === "skull-lantern"){
        itemLinks[1] = "https://i.imgur.com/2KKf29w.png";
    }

    return itemLinks;
}

function setBackground(element, link) {
    element.style.backgroundImage = "url(\"" + link[1] + "\")";
    element.style.backgroundRepeat = "no-repeat";
    element.style.backgroundPosition = "center center";
    element.style.backgroundSize = "contain";
    element.style.cursor = "pointer";
    element.addEventListener(
        "click",
        function () {
            window.open(link[0]);
        }
    );
}

function removeUpgradeLevel(item) {
    if (item === "Pyromancy Flame 15") {
        item = "Pyromancy Flame Upgraded";
    }
    item = item.replace(/[0-9]/g, '');
    item = item.trim();
    return item;
}