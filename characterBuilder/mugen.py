import json
import string
import time
from watchdog.observers import Observer
from watchdog.events import FileSystemEventHandler

CHARACTER_DETAILS = {}

stats = {}
armors = {}
weapons = {}
rings = {}

class MonitoringHandler(FileSystemEventHandler):
    def on_modified(self, event):
        print(event.src_path)
        if(event.src_path == "./BuildExport.txt"):
            fileIn = open("BuildExport.txt", 'r')
            data = fileIn.read()
            fileIn.close()

            dataArr = data.split("\n")[2:]
            statArr = dataArr[:8]
            weaponArr = dataArr[9:13]
            armorArr = dataArr[14:19]
            ringArr = dataArr[20:23]

            for stat in statArr:
                temp = stat.split(":")
                temp[0] = temp[0].upper()
                temp[1] = temp[1].strip()
                if temp[1].isnumeric() == True:
                    temp[1] = int(temp[1])
                stats[temp[0]] = temp[1]

            CHARACTER_DETAILS["stats"] = stats

            for weapon in weaponArr:
                temp = weapon.split(":")
                temp[0] = temp[0].upper()
                temp[1] = temp[1].strip()
                weapons[temp[0]] = temp[1]

            CHARACTER_DETAILS["weapons"] = weapons

            armors["head"] = armorArr[1]
            armors["chest"] = armorArr[2]
            armors["gloves"] = armorArr[3]
            armors["legs"] = armorArr[4]

            CHARACTER_DETAILS["armor"] = armors

            rings["ring1"] = ringArr[1]
            rings["ring2"] = ringArr[2]

            CHARACTER_DETAILS["rings"] = rings

            characterBackup = open("CharacterDetails.dat", "w")
            fileout = open("../scripts/detailBuilder.js", "w")

            characterBackup.write(json.dumps(CHARACTER_DETAILS))
            fileout.write("var CHARACTER_DETAILS =")
            fileout.write(json.dumps(CHARACTER_DETAILS))

            characterBackup.close()
            fileout.close()

event_handler = MonitoringHandler()
observer = Observer()
observer.schedule(event_handler, path = "./")
observer.start()
while True:
    try:
        pass
    except KeyboardInterrupt:
        observer.stop()